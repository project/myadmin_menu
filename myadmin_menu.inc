<?php

/**
 * The key function that builds the menu links whenever there is a menu rebuild.
 */
 /*

/**
 * Form builder function for module settings.
 */
function myadmin_menu_theme_settings() {
  $form['myadmin_menu_margin_top'] = array(
    '#type' => 'checkbox',
    '#title' => t('Adjust top margin'),
    '#default_value' => variable_get('myadmin_menu_margin_top', 1),
    '#description' => t('If enabled, the site output is shifted down approximately 20 pixels from the top of the viewport to display the My Admin Menu. If disabled, some absolute- or fixed-positioned page elements may be covered by the My Admin Menu.'),
    '#weight' => -3,
  );
  $form['myadmin_menu_position_fixed'] = array(
    '#type' => 'checkbox',
    '#title' => t('Keep menu at top of page'),
    '#default_value' => variable_get('myadmin_menu_position_fixed', 0),
    '#description' => t('If enabled, the My Admin Menu is always displayed at the top of the browser viewport (even after the page is scrolled). <strong>Note: In some browsers, this setting results in a malformed page, an invisible cursor, non-selectable elements in forms, or other issues. Disable this option if these issues occur.</strong>'),
    '#weight' => -2,
  );
  $form['myadmin_menu_local_task'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show menu local tasks'),
    '#default_value' => variable_get('myadmin_menu_local_task', 1),
    '#description' => t('If enabled, the My Admin Menu will display menu local tasks, menu local action.'),
    '#weight' => -1,
  );
  $form['myadmin_menu_add_content'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use menu item "Add content"'),
    '#default_value' => variable_get('myadmin_menu_add_content', 1),
    '#description' => t('If enabled, the My Admin Menu add menu item "Add content" to menu item Content menu item.'),
    '#weight' => 0,
  );
  if (module_exists('menu')) {
    $form['myadmin_parent_menu'] = array(
      '#type' => 'select',
      '#title' => t('Menu'),
      '#options' => menu_parent_options(menu_get_menus(), array('mlid' => 0)), // return complete tree
      '#default_value' => variable_get('myadmin_parent_menu', 'management:1'),
      '#description' => t('Select the menu to display.'),
      '#weight' => 1,
    );

    $form['myadmin_type_menu'] = array(
      '#type' => 'select',
      '#title' => t('Menu type'),
      '#options' => array('sky'=>'Menu Sky','black'=>'Menu Black'),
      '#default_value' => variable_get('myadmin_type_menu', 'sky'),
      '#description' => t('Select the menu type to display.'),
      '#weight' => 2,
    );

  }

  $form = system_settings_form($form);

  return $form;
}

/**
 * Wipe the menu so it can be rebuilt from scratch.
 */
function myadmin_menu_wipe() {
  db_query("DELETE FROM {menu_links} WHERE menu_name = 'myadmin_menu'");
  menu_cache_clear('myadmin_menu');
  menu_rebuild();
}
