(function($){
Drupal.behaviors.myadminmenuAttach = {
  attach: function(context, settings) {
		if (!$('#myadmin-menu').length) {
		  return;
		}

		// Apply margin-top if enabled; directly applying marginTop doesn't work in IE.
		if (Drupal && Drupal.settings && Drupal.settings.myadmin_menu) {
		  if (Drupal.settings.myadmin_menu.margin_top) {
		    $('body').addClass('myadmin-menu');
		  }
		  if (Drupal.settings.myadmin_menu.position_fixed) {
		    $('#myadmin-menu').css('position', 'fixed');
		  }
		  // Move page tabs into administration menu.
		  if (Drupal.settings.myadmin_menu.tweak_tabs) {
		    $('ul.tabs.primary li').each(function() {
		      $(this).addClass('myadmin-menu-tab').appendTo('#myadmin-menu > ul');
		    });
		    $('ul.tabs.secondary').appendTo('#myadmin-menu > ul > li.myadmin-menu-tab.active').removeClass('secondary');
		  }
		  // Collapse fieldsets on Modules page. For why multiple selectors see #111719.
		  if (Drupal.settings.myadmin_menu.tweak_modules) {
		    $('#system-modules fieldset:not(.collapsed), #system-modules-1 fieldset:not(.collapsed)').addClass('collapsed');
		  }
		}

		// Hover emulation for IE 6.
		if ($.browser.msie && parseInt(jQuery.browser.version) == 6) {
		  $('#myadmin-menu li').hover(function() {
		    $(this).addClass('iehover');
		  }, function() {
		    $(this).removeClass('iehover');
		  });
		}

		// Delayed mouseout.
		$('#myadmin-menu li').hover(function() {
		  // Stop the timer.
		  clearTimeout(this.sfTimer);
		  // Display child lists.
		  $('> ul', this).css({left: 'auto', display: 'block'})
		    // Immediately hide nephew lists.
		    .parent().siblings('li').children('ul').css({left: '-999em', display: 'none'});
		}, function() {
		  // Start the timer.
		  var uls = $('> ul', this);
		  this.sfTimer = setTimeout(function() {
		    uls.css({left: '-999em', display: 'none'});
		  }, 400);
		});
  }
};
})(jQuery);
